//
//  Provider.swift
//  VenueKit
//
//  Created by Adam VIAUD on 13/12/2017.
//  Copyright © 2017 Adam VIAUD. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

public enum ProviderError {
    case MalformedURLError
    case URLSessionError(error: Error)
    case Non200StatusCode
    case NoDataError
    case DecodingError(error: Error)
}

enum Keys : String {
    case ClientIDKey
    case ClientSecretKey
    case LocationKey
    case VersionKey
    case LimitKey
    
    var value: String {
        switch self {
        case .ClientIDKey:
            return "client_id"
        case .ClientSecretKey:
            return "client_secret"
        case .LocationKey:
            return "ll"
        case .VersionKey:
            return "v"
        case .LimitKey:
            return "limit"
        }
    }
}

enum Config : String {
    case ClientID
    case ClientSecret
    case URLEndpoint
    case Version
    case Limit
    
    var value: String {
        switch self {
        case .ClientID:
            return "OADSGEV31LFJD2TKN0GP4LHTRDNYUHKUMAJOVTSEU03XRDYO"
        case .ClientSecret:
            return "Q2VSSPSXJTTE3X1BZ4Z3KT0EGUV0II1FNCZLB1SWGMT4NM5F"
        case .URLEndpoint:
            return "https://api.foursquare.com/v2/venues/search"
        case .Version:
            return "20170101"
        case .Limit:
            return "5"
        }
    }
}

public protocol VenueProviderDelegate: class {
    func providerDidRefreshWithSuccess()
    func providerDidRefreshWithError(error: ProviderError)
}

public class VenueProvider : NSObject {
    let session: URLSession
    let locationManager: CLLocationManager
    let distanceFormatter: MKDistanceFormatter

    var displayedVenues: [Venue]?
    var lastPosition: CLLocation?
    
    public weak var delegate: VenueProviderDelegate?
    
    public override init() {
        self.session = URLSession.shared
        self.locationManager = CLLocationManager()
        self.distanceFormatter = MKDistanceFormatter()
        super.init()
        self.distanceFormatter.units = .metric
        self.locationManager.delegate = self
    }
}

extension VenueProvider {
    public func refreshVenuesForUsersLocation(distanceFilter: CLLocationDistance) {
        self.locationManager.distanceFilter = distanceFilter
        self.locationManager.startUpdatingLocation()
    }
    
    public func stopRefreshingVenues() {
        self.locationManager.stopUpdatingLocation()
    }
    
    func prepareVenueSearch(location: CLLocation) {
        searchVenues(location: location) { [unowned self] venues, error in
            if let error = error {
                self.delegate?.providerDidRefreshWithError(error: error)
            }
            else {
                self.displayedVenues = venues
                self.delegate?.providerDidRefreshWithSuccess()
            }
        }
    }
    
    func searchVenues(location: CLLocation, completionHandler: @escaping ([Venue]?, ProviderError?) -> Void) {
        var urlComponents = URLComponents(string: Config.URLEndpoint.value)
        var items: [URLQueryItem] = []
        
        items.append(contentsOf: retrieveDefaultQueryItems())
        
        let ll = "\(location.coordinate.latitude),\(location.coordinate.longitude)"
        items.append(URLQueryItem(name: Keys.LocationKey.value, value: ll))
        
        urlComponents?.queryItems = items
        
        if let finalUrl = urlComponents?.url {
            let urlRequest = URLRequest(url: finalUrl)
            
            self.session.dataTask(with: urlRequest, completionHandler: { data, response, sessionError in
                if let error = sessionError {
                    DispatchQueue.main.async {
                        completionHandler(nil, .URLSessionError(error: error))
                    }
                } else {
                    if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                        if let data = data {
                            let decoder = JSONDecoder()
                            do {
                                let foursquareResponse = try decoder.decode(FoursquareResponse.self, from: data)
                                let venues = foursquareResponse.response.venues
                                DispatchQueue.main.async {
                                    completionHandler(venues, nil)
                                }
                            } catch {
                                DispatchQueue.main.async {
                                    completionHandler(nil, .DecodingError(error: error))
                                }
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                completionHandler(nil, .NoDataError)
                            }
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            completionHandler(nil, .Non200StatusCode)
                        }
                    }
                }
            }).resume()
        }
        else {
            completionHandler(nil, .MalformedURLError)
        }
    }
}

extension VenueProvider {
    public func requestAuthorization() {
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    public func didUserAuthorized() -> Bool {
        switch CLLocationManager.authorizationStatus() {
        case .denied, .notDetermined, .restricted:
            return false
        case .authorizedAlways, .authorizedWhenInUse:
            return true
        }
    }
    
    public func isAuthorizationStatusDetermined() -> Bool {
        return CLLocationManager.authorizationStatus() != .notDetermined
    }
}

extension VenueProvider {
    func retrieveDefaultQueryItems() -> [URLQueryItem] {
        return [URLQueryItem(name: Keys.ClientIDKey.value, value: Config.ClientID.value),
                URLQueryItem(name: Keys.ClientSecretKey.value, value: Config.ClientSecret.value),
                URLQueryItem(name: Keys.VersionKey.value, value: Config.Version.value),
                URLQueryItem(name: Keys.LimitKey.value, value: Config.Limit.value)]
    }
}

extension VenueProvider : UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.displayedVenues?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        if let venue = self.displayedVenues?[indexPath.row] {
            customizeCell(cell: cell, withVenue: venue)
        }
        
        return cell
    }
    
    func customizeCell(cell: UITableViewCell, withVenue venue: Venue) {
        cell.textLabel?.text = venue.name
        
        if let userPosition = self.lastPosition {
            let venuePosition = CLLocation(latitude: venue.location.lat, longitude: venue.location.lng)
            let distance = venuePosition.distance(from: userPosition)
            cell.detailTextLabel?.text = self.distanceFormatter.string(fromDistance: distance)
        } else {
            cell.detailTextLabel?.text = "\(venue.location.lat),\(venue.location.lng)"
        }
    }
}

extension VenueProvider : CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.lastPosition = location
            prepareVenueSearch(location: location)
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("Authorization status changed to : \(status.rawValue)")
    }
}
