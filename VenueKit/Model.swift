//
//  Model.swift
//  VenueKit
//
//  Created by Adam VIAUD on 13/12/2017.
//  Copyright © 2017 Adam VIAUD. All rights reserved.
//

import Foundation

public struct Venue : Decodable {
    public struct VenueLocation : Decodable {
        public let lat: Double
        public let lng: Double
    }
    
    public let id: String
    public let name: String
    public let location: VenueLocation
}

public struct FoursquareResponse : Decodable {
    public struct ResponseData : Decodable {
        public let venues: [Venue]
    }
    
    public let response: ResponseData
}
