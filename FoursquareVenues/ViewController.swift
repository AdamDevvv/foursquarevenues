//
//  ViewController.swift
//  FoursquareVenues
//
//  Created by Adam VIAUD on 13/12/2017.
//  Copyright © 2017 Adam VIAUD. All rights reserved.
//

import UIKit
import VenueKit

class ViewController: UITableViewController {
    
    let provider = VenueProvider()
    
    var isShowingVenues = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.tableView.dataSource = self.provider
        self.provider.delegate = self
        
        handleAuthorization()
        
        refreshVenues()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions Menu
    
    @IBAction func showActionsMenu(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Actions", message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Annuler", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let refreshAction = UIAlertAction(title: isShowingVenues ? "Arrêter le rafraichissement" : "Rafraîchir les Venues", style: .default) { [unowned self] _ in
            if self.isShowingVenues {
                self.stopRefreshing()
            }
            else {
                self.refreshVenues()
            }
        }
        alertController.addAction(refreshAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension ViewController {
    func handleAuthorization() {
        self.provider.requestAuthorization()
        
        if self.provider.isAuthorizationStatusDetermined() && self.provider.didUserAuthorized() == false {
            print("Notify user that he has to enable location services for this app")
        }
    }
    
    func refreshVenues() {
        self.isShowingVenues = true
        self.provider.refreshVenuesForUsersLocation(distanceFilter: 50.0)
    }
    
    func stopRefreshing() {
        self.isShowingVenues = false
        self.provider.stopRefreshingVenues()
    }
}

extension ViewController : VenueProviderDelegate {
    func providerDidRefreshWithSuccess() {
        self.tableView.reloadData()
    }
    
    func providerDidRefreshWithError(error: ProviderError) {
        print("Display appropriate alerts based on error : \(error)")
    }
}
